# Install OpenLDAP Server. 
```bash
[root@ldapserver ~]# yum -y install openldap-servers openldap-clients migrationtools
[root@ldapserver ~]# cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG
[root@ldapserver ~]# chown ldap. /var/lib/ldap/DB_CONFIG
[root@ldapserver ~]# systemctl start slapd
[root@ldapserver ~]# systemctl enable slapd 
```

# Set OpenLDAP admin password. 
  - generate encrypted password

```bash
[root@ldapserver ~]# slappasswd      
New password:
Re-enter new password:
{SSHA}xxxxxxxxxxxxxxxxxxxxxxxx
```

- Create chroot modify config

```bash
[root@ldapserver ~]# vim chrootpw.ldif
```
 -!!!specify the password generated above for "olcRootPW" section!!!
```bash
dn: olcDatabase={0}config,cn=config
changetype: modify
add: olcRootPW
olcRootPW: {SSHA}xxxxxxxxxxxxxxxxxxxxxxxx
```

- Apply changes
```bash
[root@ldapserver ~]# ldapadd -Y EXTERNAL -H ldapi:/// -f chrootpw.ldif 
```


# Import basic Schemas. 
```bash
[root@ldapserver ~]# ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/cosine.ldif 
[root@ldapserver ~]# ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/nis.ldif 
[root@ldapserver ~]# ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/inetorgperson.ldif 
```


# Set your domain name on LDAP DB.
  - generate directory manager's password 

```bash
[root@ldapserver ~]# slappasswd          
New password:
Re-enter new password:
{SSHA}xxxxxxxxxxxxxxxxxxxxxxxx 
```
 -!!!generate certificate if you need ;)!!!
```bash
openssl req -new -x509 -nodes -out /etc/pki/tls/certs/ldapserver.pem \
-keyout /etc/pki/tls/certs/ldapserverkey.pem -days 365
```
- Create chdomain modify config
```bash
[root@dlp ~]# vim chdomain.ldif 
```
- !!!replace to your own domain name for "dc=***,dc=***" section!!!
- !!!specify the password generated above for "olcRootPW" section!!!

```bash
dn: olcDatabase={1}monitor,cn=config
changetype: modify
replace: olcAccess
olcAccess: {0}to * by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth"
  read by dn.base="cn=Manager,dc=ldapserver,dc=com" read by * none

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcSuffix
olcSuffix: dc=ldapserver,dc=com

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcRootDN
olcRootDN: cn=Manager,dc=ldapserver,dc=com

dn: olcDatabase={2}hdb,cn=config
changetype: modify
add: olcRootPW
olcRootPW: {SSHA}xxxxxxxxxxxxxxxxxxxxxxxx

dn: olcDatabase={2}hdb,cn=config
changetype: modify
add: olcAccess
olcAccess: {0}to attrs=userPassword,shadowLastChange by
  dn="cn=Manager,dc=ldapserver,dc=com" write by anonymous auth by self write by * none
olcAccess: {1}to dn.base="" by * read
olcAccess: {2}to * by dn="cn=Manager,dc=ldapserver,dc=com" write by * read
```
- Apply changes
```bash
ldapmodify -Y EXTERNAL -H ldapi:/// -f chdomain.ldif
```

- Create basedomain modify config
```bash
[root@ldapserver ~]# vim basedomain.ldif 
```
 - !!!replace to your own domain name for "dc=***,dc=***" section!!!
```bash
dn: dc=ldapserver,dc=com
objectClass: top
objectClass: dcObject
objectclass: organization
o: Ventsislav
dc: ldapserver

dn: cn=Manager,dc=ldapserver,dc=com
objectClass: organizationalRole
cn: Manager
description: Directory Manager

dn: ou=People,dc=ldapserver,dc=com
objectClass: organizationalUnit
ou: People

dn: ou=Group,dc=ldapserver,dc=com
objectClass: organizationalUnit
ou: Group
```

- Apply changes

```bash
[root@ldapserver ~]# ldapadd -x -D cn=Manager,dc=ldapserver,dc=com -W -f basedomain.ldif
Enter LDAP Password: # directory manager's password
```


# If Firewalld is running, allow LDAP service. LDAP uses 389/TCP.
```bash
[root@ldapserver ~]# firewall-cmd --add-service=ldap --permanent
[root@ldapserver ~]# firewall-cmd --reload
```
