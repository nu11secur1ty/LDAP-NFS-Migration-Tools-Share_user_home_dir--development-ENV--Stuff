# Ldap Client Configuration to use LDAP Server
 - packages
```bash
yum install -y openldap-clients nss-pam-ldapd
```
- Settings
```bash
 authconfig-tui
```
- Steps to follow for LDAP Authentication:
```bash
1. Put '*' Mark on "Use LDAP"
2. Put '*' Mark on "Use LDAP Authentication"
3. Select "Next" and Enter.
4. Enter the server field as "ldap://linux1.learnitguide.net/"
5. Enter the Base DN Field as "dc=learnitguide,dc=net"
6. Select "OK" and Enter
```

- test LDAP mount user directory:
```bash
[root@ldapclient ~]# getent passwd cent
cent:*:1000:1000:Cent:/home/cent:/bin/bash
```

# Mount the LDAP Users Home Directory.
```bash
vim /etc/fstab
ldapserver.com:/home   /home   auto  defaults    0 0
```

