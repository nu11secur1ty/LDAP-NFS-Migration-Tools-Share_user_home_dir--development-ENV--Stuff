# Install NFS on Server
```bash
 yum install nfs-utils
```
- Enable services:
```bash
systemctl enable rpcbind
systemctl enable nfs-server
systemctl enable nfs-lock
systemctl enable nfs-idmap
systemctl start rpcbind
systemctl start nfs-server
systemctl start nfs-lock
systemctl start nfs-idmap
```
- Share and mount directory * in /etc/exports
```bash
vim /etc/exports
```
 - add
```bash
/home/ *(rw,sync,no_root_squash,no_all_squash)
```
- restart NFS:

```bash
systemctl restart nfs-server
```
# Done ;)
