# Add a new user
  - generate encrypted password         
```bash
[root@dlp ~]# slappasswd
```
New password:
Re-enter new password:
{SSHA}xxxxxxxxxxxxxxxxx

- Create ldap user modify config
```bash
[root@dlp ~]# vim ldapuser.ldif 
```
 - !!!replace to your own domain name for "dc=***,dc=***" section and generated password!!!
```bash
dn: uid=cent,ou=People,dc=ldapserver,dc=com
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
cn: Cent
sn: Linux
userPassword: {SSHA}xxxxxxxxxxxxxxxxx
loginShell: /bin/bash
uidNumber: 1000
gidNumber: 1000
homeDirectory: /home/cent

dn: cn=cent,ou=Group,dc=ldapserver,dc=com
objectClass: posixGroup
cn: Cent
gidNumber: 1000
memberUid: cent
```
- Apply changes using LDAP Manager Password:

```bash
[root@ldapserver ~]# ldapadd -x -D cn=Manager,dc=ldapserver,dc=com -W -f ldapuser.ldif
```

 # Create the same user on the system which will be controlled from LDAP 

```bash
useradd cent
echo "userpass" | passwd --stdin cent
```
 - Filter out these user from /etc/passwd to another file:
```bash
 grep ":10[0-9][0-9]" /etc/passwd > /root/passwd
 grep ":10[0-9][0-9]" /etc/group > /root/group
```
- Prepare for migration
```bash
cd /usr/share/migrationtools/
vim migrate_common.ph
```
 - edit:
```bash
    $DEFAULT_MAIL_DOMAIN = "ldapserver.com";
    $DEFAULT_BASE = "dc=ldapserver,dc=com";
    $EXTENDED_SCHEMA = 1;
```
 - Migrate user credentials from "password and "group"

```bash
./migrate_passwd.pl /root/passwd /root/users.ldif
./migrate_group.pl /root/group /root/groups.ldif
```

- Modify this change using Manager Password:
```bash
ldapadd -x -W -D "cn=Manager,dc=ldapserver,dc=com" -f /root/basedomain.ldif 
ldapadd -x -W -D "cn=Manager,dc=ldapserver,dc=com" -f /root/users.ldif 
ldapadd -x -W -D "cn=Manager,dc=ldapserver,dc=com" -f /root/groups.ldif
``` 

 - move to ldapserver group and give permission for "cent" directory
```bash
 usermod -a -G ldapserver cent
 chmod 775 /home/cent
```
 - Test configuration:
```bash
ldapsearch -x cn=cent -b dc=ldapserver,dc=com
```
- Test all configuration:
```bash  
ldapsearch -x -b 'dc=ldapserver,dc=com' '(objectclass=*)'
```


# Well done ;)

